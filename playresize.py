import torch
from torch.autograd import Variable

n_batch = 10
n_seq = 32
n_feature = 60

a = torch.rand(10,32,60)
a = Variable(a)

# pack to 2 dim
a2 = a.resize(n_batch*n_seq, n_feature)
a3 = a2.resize(n_batch, n_seq, n_feature)

print torch.sum(a.data.eq(a3.data))