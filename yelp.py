import json
YELP_REVIEW_PATH = '../data/yelp/small.json'
from nltk import tokenize

def get_review(line_json):
    obj = json.loads(line_json)
    text = obj['text']
    stars = obj['stars']
    sentences = tokenize.sent_tokenize(text)
    return sentences, stars

def write_to_file(file_path):
    f_to_write = open(file_path, 'w');
    with open(YELP_REVIEW_PATH, 'r') as f:
        for line in f:
            sentences, stars = get_review(line)
            for sent in sentences:
                f_to_write.write(sent+ ' ' + str(stars) + '\n')


def print_on_screen():
    with open(YELP_REVIEW_PATH, 'r') as f:
        for line in f:
            sentences = get_review(line)
            for sent in sentences:
                f.write(sent+'/n')

def put_in_array():
    review_group_list = []
    with open(YELP_REVIEW_PATH, 'r') as f:
        for line in f:
            sentences, stars = get_review(line)
            review_group = {}
            review_group['sentences'] = sentences
            review_group['stars'] = stars
            review_group_list.append(review_group)
    return review_group_list

if __name__ == "__main__":
    review_groups = put_in_array()
    f = open('out.json', 'w');
    json.dump(review_groups,f)

    print 'done'
