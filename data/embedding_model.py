import gensim
import numpy as np
from sklearn.externals import joblib


class EmbeddingModel:
    def __init__(self, pretrain_path, embedding_type, EMPTY_path = None, UNKNOWN_path = None):
        """
        :param pretrain_path: path to pretrained set
        :param embedding_type: word2vec or glove
        """

        self.embedding_type=embedding_type
        self.W2V = 'word2vec'
        self.GLOVE = 'glove'
        self.DUMMY = 'dummy'
        self.model = 'dummy'

        if (EMPTY_path == None):
        # initial EMPTY
            if (embedding_type=='word2vec'):
                print 'initial w2v EMPTY'
                self.EMPTY = np.random.uniform(-0.1, 0.1, 300).astype(np.float32)
                joblib.dump(self.EMPTY, 'NTISentiment_w2v.empty')
            elif (embedding_type=='glove'):
                print 'initial glove EMPTY'
                self.EMPTY = np.random.uniform(-0.1, 0.1, (1, 300)).astype(np.float32)
                self.UNKNOWN = np.random.uniform(-0.1, 0.1, (1, 300)).astype(np.float32)
                joblib.dump(self.EMPTY, 'glove.empty')
                joblib.dump(self.UNKNOWN, 'glove.unknown')
        elif (EMPTY_path != None):
            print 'load EMPTY vector from ',EMPTY_path
            self.EMPTY = joblib.load(EMPTY_path)
            self.UNKNOWN = joblib.load(UNKNOWN_path)


        if (embedding_type=='word2vec'):
            model = gensim.models.Word2Vec.load_word2vec_format(pretrain_path,
                                                                binary=True)  # C binary format
            self.model = model
        elif (embedding_type=='glove'):
            with open(pretrain_path, 'r') as f:
                vectors = {}
                for line in f:
                    vals = line.rstrip().split(' ')
                    vectors[vals[0]] = np.array(vals[1:], ndmin=2, dtype=np.float32)
                vectors['<EMPTY>'] = self.EMPTY
                self.model = vectors



    def embedding(self, word):
        model = self.model
        if (self.embedding_type==self.W2V):
            if word in model.wv.vocab:
                return np.asarray(model[word], dtype='float32')
            elif (word == '<EMPTY>'):
                return self.EMPTY
            else:
                return np.zeros(300, dtype= 'float32')
        elif (self.embedding_type==self.GLOVE):
            try:
                vec = model[word]
            except:
                # vec = np.zeros((1, 300), dtype=np.float32) # TODO: random for unknown embedding
                vec = self.UNKNOWN
            return vec
        elif self.embedding_type==self.DUMMY:
            return np.random.uniform(-0.1, 0.1, (1, 300)).astype(np.float32)

