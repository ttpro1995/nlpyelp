import time
import json
import nltk
from nltk import tokenize
from joblib import Parallel, delayed

def get_review(line_json):
    """
    Json object of review => sentences and stars
    :return:
    """
    obj = json.loads(line_json)
    text = obj['text']
    stars = obj['stars']
    sentences = tokenize.sent_tokenize(text)
    review_group = {}
    review_group['sentences'] = sentences
    review_group['stars'] = stars
    return review_group


def preprocessed_dataset_2(batch, saved_path=None):
    """
    get paragraph and stars from yelp, separate sentences and save as json
    :param yelp_path: yelp review dataset
    :param saved_path: save preprocessed json
    :return:
    """

    review_group_list = Parallel(n_jobs=8, verbose=1)(delayed(get_review)(line) for line in batch)


    if saved_path is not None:
        saved_file = open(saved_path, 'w')
        json.dump(review_group_list, saved_file)
        # print ('saved json to ', saved_file.name)

def preprocess_multi_part(n):
    """
    Data too big, let divide into smaller part
    :param n:
    :return:
    """
    f = open('../../yelp/yelp_academic_dataset_review.json')
    lines = f.readlines()
    full_size = len(lines)
    part_size = len(lines)/n
    current_index = 0
    counter = 0
    print ('len per part', part_size)
    while current_index < full_size:
        end_index = current_index + part_size
        part = lines[current_index:end_index]
        preprocessed_dataset_2(part, 'preprocess_output/outpart'+str(counter)+'.json')
        current_index = end_index
        counter +=1

if __name__ == '__main__':
    # f = open('../../yelp/yelp_academic_dataset_review.json')
    # f = open('../../yelp/small.json')
    # lines = f.readlines()
    # preprocessed_dataset_2(lines, 'outsmall.json')
    preprocess_multi_part(100)
