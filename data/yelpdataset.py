import numpy as np
import json
from nltk import tokenize
class Dataset:
    def __init__(self, yelp_path = None, sentence_padding = False):
        self.embedding_model = None
        self.yelp_path = yelp_path # preprocess
        self.sentence_padding = sentence_padding # active padding function
        print ('yelp path ', yelp_path)

    def set_embedding_model(self, embedding_model):
        self.embedding_model = embedding_model

    def drop_embedding_model(self):
        self.embedding_model = None

    def load_dataset(self, yelp_path):
        """
        load from preprocessed path
        :param yelp_path: review json from yelp
        :return: 
        groups, stars (for each group)
        """
        json_obj = json.load(open(yelp_path, 'r'))
        return json_obj


    def get_review(self, line_json):
        """
        Json object of review => sentences and stars 
        :return: 
        """
        obj = json.loads(line_json)
        text = obj['text']
        stars = obj['stars']
        sentences = tokenize.sent_tokenize(text)
        return sentences, stars

    def padding_sentence(self, sentence_token, length = 32):
        """
        Padding <EMPTY> before sentence to make fix length sentence
        :param sentence_token: "this is a string".split()
        :param length: default 32
        :return: list of word
        """

        words = sentence_token[:length]
        if (len(words)<length):
            words = ['<EMPTY>'] * (length - len(words)) + words
        return words

    def create_embedding_dataset(self, is_binary = False):
        json_obj = self.load_dataset(self.yelp_path)

        embedding_model = self.embedding_model
        x = []
        y = []

        for unit in json_obj:
            l = unit['stars']
            if is_binary:
                if (l==2):
                    continue # discard neutral
                if (l >=3):
                    l = 1 # good
                elif(l<=1):
                    l = 0 # bad
                y.append(l)
            else:
                y.append(l)

            comment_group = unit['sentences']
            group_emb = []
            for sentence in comment_group:
                sentence_vec = []
                sentence_token = sentence.split()
                if self.sentence_padding:
                    sentence_token = self.padding_sentence(sentence_token)
                for word in sentence_token:
                    vec = embedding_model.embedding(word)  # some word will be ignore because no embedding
                    sentence_vec.append(vec)
                group_emb.append(sentence_vec)
            group_emb = np.array(group_emb)
            x.append(group_emb)

        return (x, y)

class Batch:
    def __init__(self, x = None, y = None, dub_data = False, load_dir = None):
        if load_dir is None:
            self.x = np.array(x)
            self.y = np.array(y)

        else:
            self.x = np.load(load_dir+'/batch_x.npy')
            self.y = np.load(load_dir+'/batch_y.npy')
            print ('Batch load from ', load_dir)
        print ('shape x', self.x.shape, 'type', self.x.dtype , 'size ', self.x.nbytes)
        print ('shape y', self.y.shape, 'type', self.y.dtype, 'size', self.y.nbytes)
        # gi vay ?
        # dang code ma :v ,
        self.n_sample = len(y)
        assert len(y) == len(x)
        self.left_idx = 0
        self.dub_data = dub_data

    def save_npy(self,dir):
        np.save(dir+'/'+'batch_x', self.x)
        np.save(dir+'/'+'batch_y', self.y)    

    def get_batch(self, batch_size = 64):
        """
        get a batch of batch_size
        :param batch_size: default 32
        :return: x, y as list
        """
        right_idx = batch_size+self.left_idx
        x = self.x[self.left_idx:right_idx]
        y = self.y[self.left_idx:right_idx]
        if (self.dub_data == True): # dublicate data for tree matching
            x = np.concatenate([x, x])
        y = np.asarray(y, dtype='int64')
        self.left_idx = right_idx # update left idx for next batch
        return (x, y)

    def has_batch(self):
        """
        Check if we have any batch left
        :return:
        """
        if (self.left_idx < self.n_sample):
            return True
        return False

    def reset_batch(self, shuffle = False):
        self.left_idx = 0

        # permutation dataset
        if (shuffle == True):
            shuffle_len = self.x.shape[0]
            print ('Shuffle len ', shuffle_len)
            per = np.random.permutation(shuffle_len)
            self.x = self.x[per]
            self.y = self.y[per]


