from yelpdataset import Dataset
from yelpdataset import Batch
from embedding_model import EmbeddingModel
import torch
import numpy as np
from models.GroupLSTMsentiment import GroupLSTMsentiment

def meow1():
    ds = Dataset('../../data/yelp/small.json', sentence_padding=True)
    groups, labels = ds.load_dataset(ds.yelp_path)
    dummy_emb_model = EmbeddingModel(None, 'dummy')
    ds.set_embedding_model(dummy_emb_model)
    x, y = ds.create_embedding_dataset()
    model = GroupLSTMsentiment(300, 300, 300, 2)
    batch = Batch(x, y)
    x_b, y_b = batch.get_batch(1)
    x_tensor = torch.from_numpy(x_b[0])
    y_tensor = torch.from_numpy(y_b)
    model.train(x_tensor, y_tensor)

if __name__ == "__main__":

    ds = Dataset('outsmall.json', sentence_padding=True)
    dummy_emb_model = EmbeddingModel(None, 'dummy')
    ds.set_embedding_model(dummy_emb_model)
    x, y = ds.create_embedding_dataset()
    print 'break'