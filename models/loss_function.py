"""File that defines instance similarity. In this case, instances are sentences."""
import numpy as np
import torch
import torch.nn.functional as F
from torch.autograd import Variable

def get_matrix_cos_np(X):
    """https://en.wikipedia.org/wiki/Cosine_similarity in numpy"""
    similarity = np.dot(X, X.T)

    # squared magnitude of preference vectors (number of occurrences)
    square_mag = np.diag(similarity)

    # inverse squared magnitude
    inv_square_mag = 1 / square_mag

    # if it doesn't occur, set it's inverse magnitude to zero (instead of inf)
    inv_square_mag[np.isinf(inv_square_mag)] = 0

    # inverse of the magnitude
    inv_mag = np.sqrt(inv_square_mag)

    # cosine similarity (elementwise multiply by inverse magnitudes)
    cosine = similarity * inv_mag
    cosine = cosine.T * inv_mag
    return cosine

def loss_function2(ygroup, y_label, ysentence, hsentence):
    # ygroup(n_group, n_output)
    # ysentence(n_sentence, n_output)
    # hsentence(n_sentence, n_embedding)

    ygroup = ygroup.squeeze(1)
    ysentence = ysentence.squeeze(1)

    n = ysentence.size(0)
    k = ygroup.size(0)
    l = 1
    similarity_matrix = get_matrix_cos(hsentence) # (4 4) #TODO: backward not implement error
    delta = get_delta(ysentence) # (1 4 1)

    s1 = similarity_matrix*delta
    s1 = s1.sum()

    s2 = (ygroup - y_label)*(ygroup - y_label)
    s2 = torch.mean(s2) * l

    loss = s1 + s2
    return loss


def loss_function(hsentences, predstars, stars):
    n = hsentences.size()[0]
    k = stars.size()[0]
    l = 1

    similarity_matrix = get_matrix_cos(hsentences)
    pred = F.sigmoid(hsentences)
    delta = get_delta(pred)
    s = similarity_matrix*delta

    s = torch.sum(s, 0) # sum by dim 0

    stars_onehot = to_onehot(stars, 5)
    stardelta = predstars - stars_onehot
    stardelta = stardelta*stardelta
    s2 = torch.sum(stardelta, 0)

    result = (1/(n*n))*s + (l/k)*s2
    return result

def to_onehot(y, n_classes):
    """
    :param y: 1d array
    :return: onehot 2d array
    """
    onehot = torch.eye(n_classes).index(y)
    return onehot


def get_delta_old(Y):
    """
    
    :param Y: (n_group, nclasses)
    :return: 
    """
    n_group = Y.size()[0]
    n_classes = Y.size()[1]

    Y = torch.unsqueeze(Y, 0) # (1, n_group, n_classes)
    Y_np = Y.numpy()
    Y_transpose = torch.from_numpy(Y_np.T)
    # d = Y.size()[1]
    Y_expand = Y.expand(n_classes, n_group, n_classes)
    Y_transponse_expand =  Y_transpose.expand(n_classes, n_group, n_classes)
    print Y_expand
    print Y_transponse_expand
    delta = Y_expand - Y_transpose.expand(n_classes, n_group, n_classes)
    delta = delta*delta
    return delta

def get_delta(y):
    """
`
    :param y: 1D vector
    :return:
    """
    d = y.size()[0]
    y = y.unsqueeze(0)
    y_t = y.t()
    delta = y.expand(d,d) - y_t.expand(d,d)
    return delta



def get_matrix_cos(X):
    """https://en.wikipedia.org/wiki/Cosine_similarity"""
    # similarity = np.dot(X, X.T)
    similarity = X.mm(X.t())

    # squared magnitude of preference vectors (number of occurrences)
    # square_mag = np.diag(similarity)
    square_mag = similarity.diag()

    # inverse squared magnitude
    inv_square_mag = 1 / square_mag

    # if it doesn't occur, set it's inverse magnitude to zero (instead of inf)
    # inv_square_mag[np.isinf(inv_square_mag)] = 0
    # inv_square_mag[inv_square_mag>=1E307] = 0 # TODO: backward NOT implement here

    # inverse of the magnitude
    # inv_mag = np.sqrt(inv_square_mag)
    inv_mag = inv_square_mag.sqrt()

    # cosine similarity (elementwise multiply by inverse magnitudes)
    cosine = similarity * inv_mag.expand_as(similarity)
    cosine = cosine.t() * inv_mag.expand_as(similarity)
    return cosine

def test_get_delta():
    aa = torch.Tensor([[1, 2, 3, 4], [2, 3, 4, 1], [3, 4, 1, 2]])
    x = get_delta(aa)
    print x

if __name__=="__main__":
    #
    # print 'onehot'
    # y = torch.LongTensor([1,3,4,2,0])
    # onehot = to_onehot(y, 5)
    # print onehot



    print 'lost function'
    y_group = torch.Tensor([[1],[0]])
    ysentence = torch.Tensor([[0],[1],[0],[0]])
    hsentence = torch.rand(4, 30)
    y_label = torch.Tensor([1,0])
    loss = loss_function2(y_group, y_label,ysentence,hsentence)
    print loss
    print 'break'
    a = torch.Tensor([1,2,3,4])
    # x  = get_delta(a)


    # print x
    # test_get_delta()
    print 'break2'