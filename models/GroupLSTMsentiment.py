import torch.nn as nn
import torch
from torch.autograd import Variable
import torch.nn.functional as F
import numpy as np
import torch.optim as optim
from loss_function import loss_function2

class GroupLSTMsentiment(nn.Module):
    """
    For sentence - label pair
    """
    def __init__(self, indim, hiddim_lstm, hiddim_mlp, n_classes, is_cuda = False):
        super(GroupLSTMsentiment,self).__init__()
        self.is_cuda = is_cuda
        if (is_cuda):
            self.cuda()
        self.indim = indim
        self.hiddim_lstm = hiddim_lstm
        self.hiddim_mlp = hiddim_mlp

        self.sentence_lstm = nn.LSTM(input_size=indim, hidden_size=hiddim_lstm, num_layers=1, batch_first=True)

        # two-layer MLP
        self.l1 = nn.Linear(hiddim_lstm, n_classes)

        self._optimizer = optim.Adam(self.parameters(), lr=0.001, weight_decay=0.00003)
        # self._criterion = nn.CrossEntropyLoss()
        self._criterion = loss_function2

        if is_cuda:
            self.sentence_lstm = self.sentence_lstm.cuda()
            self.l1 = self.l1.cuda()
            # self._criterion = self._criterion.cuda()


    def forward(self, input, training = False):
        """

        :param input: (n_group, n_sentences_per_batch, n_word_per_sentence, feature)
        :param training:
        :return:
        ygroup (n_group, output)
        ysentence (n_sentence, n_output)
        hsentence (n_sentence, n_embedding)
        """

        #TODO: delete the simulate
        input = input.unsqueeze(0) # n_batch = 1

        n_batch = input.size(0)
        n_sen_per_batch = input.size(1)
        n_word_per_batch = input.size(2)
        n_feature = input.size(3)

        # stack sentence of all batch to forward whole batch
        inputb = input.resize(n_batch*n_sen_per_batch, n_word_per_batch, n_feature)

        o, hn = self.sentence_lstm(F.dropout(inputb, p=0.2, training=training))

        # TODO: feed here to group LSTM
        hsentence = hn[0]   # (1, n_sen_per_batch, n_feature)
        hsentence = hsentence.squeeze(0)
        ysentence = F.sigmoid(self.l1(F.dropout(hsentence, p=0.2, training=training))) # (total_sen, n_classes)

        n_feature = ysentence.size(1)

        ysentence_g = ysentence.resize(n_batch, n_sen_per_batch, n_feature)

        ygroup = ysentence_g.sum(1)/ysentence_g.size(1) # calculate h group by get average
        ygroup = ygroup.squeeze(1) # (n_batch, n_classes)

        return ygroup, ysentence, hsentence

    def train(self, x, y):
        """
        
        :param x: numpy group (for parallel)
        :param y: label for whole group
        :return: 
        """
        self._optimizer.zero_grad()

        if self.is_cuda:
            x = x.cuda()
            y = y.cuda()
        x_tensor = Variable(x)
        y_tensor = Variable(y)

        # ouput (1, n_classes) (1, 2)
        # hsentences (1, n_sentences, feature) (1, 3, 300)
        output, ysentence, hsentence = self.forward(x_tensor[:,:,0], training=True)

        # y_tensor (n_group)
        # ouput (n_group, n_classes) (1, 5)
        # hsentences (n_group, n_sentences, feature) (1, 3, 300)
        # loss = self._criterion(output, y_tensor)
        loss = self._criterion(output, y_tensor.float(), ysentence, hsentence)
        loss.backward()
        self._optimizer.step()
        return loss

    def predict(self, x):
        if self.is_cuda:
            x = x.cuda()
        x_tensor = Variable(x)
        output, ysentence, hsentence  = self.forward(x_tensor[:, :, 0])
        return output
