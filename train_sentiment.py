import numpy as np
from data import Batch
import torch
import time
from models import GroupLSTMsentiment
import CONST_ubuntu as CONST
import meowlogtool.log_util as log_util
from meowlogtool.log_util import StreamToLogger
from data import Dataset
from data import EmbeddingModel
from sklearn.model_selection import train_test_split
import sys
if __name__ == "__main__":
    # attach meow log tool
    logger = log_util.create_logger("GroupLSTM", True)
    logstream = StreamToLogger(logger)
    sys.stdout = logstream
    print '-------------Start------------------'

    ds = Dataset(CONST.yelp_small, sentence_padding=True)

    print ('embedding', CONST.embedding_path, CONST.EMBEDDING_MODE)
    embedding = EmbeddingModel(CONST.embedding_path, 'dummy')
    ds.set_embedding_model(embedding)
    print ('Done load embedding')
    x, y = ds.create_embedding_dataset(is_binary=True)
    print ('done load dataset')
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.25)
    x_train, x_dev, y_train, y_dev = train_test_split(x_train, y_train, test_size=0.1)
    print ('done Train dev test split')

    batch = Batch(x_train, y_train)
    dev_batch = Batch(x_dev, y_dev)
    test_batch = Batch(x_test, y_test)


    print ('Begin training')
    x, y = batch.get_batch()
    # x shape (64, 32, 1, 300)
    # y shape (64)
    is_cuda = True
    print ('CUDA',is_cuda)

    model = GroupLSTMsentiment(300, 300, 200, 1, is_cuda)
    start_t = time.time()
    print ('Model ', type(model).__name__)

    for e in range(1, 10):
        batch.reset_batch(shuffle=True)
        while batch.has_batch():
            input_x, input_y = batch.get_batch(batch_size=1)
            # x (64, 10, 1, 58)
            # y (64)
            x_tensor = torch.from_numpy(input_x[0])
            y_tensor = torch.from_numpy(input_y)
            loss = model.train(x_tensor, y_tensor)
        print('epoch ', e, 'loss ', loss)

        # evaluate on dev data
        total = 0
        correct = 0
        dev_batch.reset_batch(shuffle=True)
        while dev_batch.has_batch():
            input_x, input_y = dev_batch.get_batch(batch_size=1)
            # x (64, 10, 1, 58)
            # y (64)
            x_tensor = torch.from_numpy(input_x[0])
            y_tensor = torch.from_numpy(input_y)
            predicted = torch.zeros(y_tensor.size()).long()
            if is_cuda:
                y_tensor = y_tensor.cuda()
                predicted = predicted.cuda()
            pred = model.predict(x_tensor)
            predicted[pred.data >= 0.5] = 1
            total += y_tensor.size(0)
            correct += (predicted == y_tensor).sum()
        print ('dev ',correct, '/', total, 'percentage ', 100 * float(correct) / total)
        model_saved_name = 'epoch'+str(e)+'.pth'
        torch.save(model, model_saved_name)
        print ('saved', model_saved_name)



    # evaluate on test data
    total = 0
    correct = 0
    test_batch.reset_batch(shuffle=True)
    while test_batch.has_batch():
        input_x, input_y = test_batch.get_batch(batch_size=1)
        # x (64, 10, 1, 58)
        # y (64)
        x_tensor = torch.from_numpy(input_x[0])
        y_tensor = torch.from_numpy(input_y)
        predicted = torch.zeros(y_tensor.size()).long()
        if is_cuda:
            y_tensor = y_tensor.cuda()
            predicted = predicted.cuda()
        pred = model.predict(x_tensor)
        predicted[pred.data >= 0.5] = 1
        total += y_tensor.size(0)
        correct += (predicted == y_tensor).sum()
    print (correct, '/', total, 'percentage ', 100 * float(correct) / total)

    end_t = time.time()

    print ('total time', end_t - start_t)
    print '-------------DONE------------------'
